<?php

/******************************************************************************/
/* router controller **********************************************************/
/* description: it defines which page to show *********************************/
/******************************************************************************/

/* final content string variable **********************************************/
$routerContent = "";

/******************************************************************************/
/* ROUTE 1: LOGGED IN *********************************************************/
/******************************************************************************/
if($session->isLoggedIn()){

	/* ROUTE 1.1: LOG-OUT *******************************************************/
	if(isset($_POST['log-out'])){
		$session->logout(); // close session
		$routerContent = include_once "views/login_form.php"; // show form
	}
	/* ROUTE 1.2: MAIN NAVIGATION ***********************************************/
	else {

		/* view from navigator ****************************************************/
		if(isset($_POST['view'])){
			$view = $_POST['view'];
		}
		/* default home page view *************************************************/
		else {
			$view = "home";
		}

		/* set dynamic content ****************************************************/
		$routerContent .= include_once "views/$view.php";

	}
}
/******************************************************************************/
/* ROUTE 2: NOT LOGGED IN *****************************************************/
/******************************************************************************/
else {

	/* ROUTE 2.1: LOG-IN FORM SUBMITTED *****************************************/
	if(isset($_POST['log-in'])){

		/* check credentials from form ********************************************/
		$username = $_POST['username'];
		$password = $_POST['password'];
		$correctCredentials = $db->checkCredentials($username, $password);

		/* ROUTE 2.1.1: CORRECT CREDENTIALS ***************************************/
		if($correctCredentials){

			/* save session variables and show home page ****************************/
			$session->login();
			$routerContent .= include_once "views/home.php";

		}

		/* ROUTE 2.1.2: FALSE CREDENTIALS *****************************************/
		else {
			$routerContent = include_once "views/login_form.php";
			$routerContent .= "<div class='failure-message'><p>User was not found in database</p></div>";
		}

	}
	/* ROUTE 2.2: FORGOT PASSWORD ***********************************************/
	else if(isset($_GET['action']) && $_GET['action'] === 'password_recovery'){

		/* password recovery form *************************************************/
		$routerContent .= include_once "views/password_recovery.php";

	}
	/* ROUTE 2.3: LOGIN FORM ****************************************************/
	else {

		$routerContent = include_once "views/login_form.php";

	}
}

/* return final content depending of the route ********************************/
return $routerContent;

?>
