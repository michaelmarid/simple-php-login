<?php

/******************************************************************************/
/* simple php login system with basic routes **********************************/
/* author: Michail Maridakis **************************************************/
/* email: michael.marid@gmail.com *********************************************/
/******************************************************************************/

/* error reporting ************************************************************/
error_reporting(E_ALL);
ini_set("display_errors", 1);

/* create a new session model object ******************************************/
include_once "models/session_class.php";
$session = new SESSION();

/* require configuration variables ********************************************/
$config = include_once('_settings/config.php');

/* connection with database ***************************************************/
include_once "models/data_class.php";
$db = new DB_CONNECTION($config);

/* control content with router ************************************************/
$content = include_once "controllers/router.php";

?>

<!DOCTYPE html>
<html lang="el">
<head>
	<!-- meta data -->
	<meta charset="utf-8">
	<meta name="author" content="">
	<meta name="robots" content="index, follow" />
	<meta name="description" content=""/>
	<meta name="keywords" content=""/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- title & favicon -->
	<title>Title</title>
	<link rel="icon" type="image/png" href="favicon.png">

	<!-- styles -->
	<link href="static/styles/bootstrap.min.css" rel="stylesheet">
	<link href="static/styles/login.css" rel="stylesheet">
	<link href="static/styles/theme.css" rel="stylesheet">

	<!-- scripts -->
	<script type="text/javascript" src="static/scripts/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="static/scripts/bootstrap.min.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<!-- main content div -->
	<div class="container">

		<!-- dynamic content -->
		<?php echo $content ?>

	</div>

  </body>
</html>
