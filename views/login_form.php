<?php

/******************************************************************************/
/* simple login form **********************************************************/
/******************************************************************************/

return "

<div class='text-center' style='padding:50px 0'>

	<img class='img-responsive center' style='margin: 0 auto; max-height: 170px;' src='static/images/logo.png'>
	<h3 class='text-center'>Management Information System</h3>
	<br/>
	<br/>
	<div class='logo'>Log In</div>

	<!-- Main Form -->
	<div class='login-form-1'>

		<form id='login-form' class='text-left' method='post' action='index.php'>

			<div class='login-form-main-message'></div>

			<div class='main-login-form'>

				<div class='login-group'>

					<div class='form-group'>
						<label for='lg_username' class='sr-only'>Username</label>
						<input type='text' class='form-control' id='login-name' name='username' placeholder='username' required>
					</div>

					<div class='form-group'>
						<label for='lg_password' class='sr-only'>Password</label>
						<input type='password' class='form-control' id='login-pass' name='password' placeholder='password' required>
					</div>

				</div>

				<button type='submit' class='login-button' name='log-in'><i class='glyphicon glyphicon-chevron-right'></i></button>

			</div>

		</form>

		<div class='etc-login-form'>
				<p>
				<a href='index.php?action=password_recovery' class='fpass'>Password Recovery</a>
				</p>
		</div>


	</div>

	<!-- end:Main Form -->

</div>


";


?>
