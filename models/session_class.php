﻿<?php

/******************************************************************************/
/* session class **************************************************************/
/* description: start session and store global variables through pages ********/
/******************************************************************************/

class SESSION {

	/* constructor **************************************************************/
	public function __construct(){
		/* begin a session if first visit or load session variables from previous request */
		session_start();
	}

	/* check if logged in method ************************************************/
	public function isLoggedIn(){
		if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true) return true;
		else return false;
	}

	/* store session variables (if successful login) ****************************/
	public function login(){
		$_SESSION['logged_in'] = true;
	}

	/* destroy session method (logout) ******************************************/
	public function logout(){
		$_SESSION['logged_in'] = false;
		session_unset();
		session_destroy();
	}

} //class

?>
