<?php

/******************************************************************************/
/* database (mySQL) connection class ******************************************/
/* description: functions to interact with the database ***********************/
/******************************************************************************/

class DB_CONNECTION {

	private $db;

	/* pdo constructor **********************************************************/
	public function __construct ($config){
		try {
			$dbInfo = "mysql:host=" . $config['host'] . ";dbname=" . $config['dbname'] . ";charset=utf8";
			$db = new PDO($dbInfo, $config['username'], $config['password'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET UTF8"));
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db = $db;

			/* check if user table exists and if not create one with test data ******/
			$userTableExists = $this->makeStatement("CREATE TABLE IF NOT EXISTS users (id VARCHAR(20) NOT NULL PRIMARY KEY, pass BLOB NOT NULL);INSERT INTO users VALUES ('username', AES_ENCRYPT('password','mySecretSalt'))");
		}
		catch(PDOException $e) {
		    echo "Connection failed: " . $e->getMessage();
		}
	}

	/* execution for all queries method *****************************************/
	private function makeStatement($sql, $data = NULL){
		$statement = $this->db->prepare($sql);
		try {
			$statement->execute($data);
		}
		catch(Exception $e){
			$msg = "<p>You tried to run this sql: $sql</p> <p>Exception: $e</p>";
			trigger_error($msg);
		}
		return $statement;
	}

	/* check credentials method *************************************************/
	public function checkCredentials($username, $password){
		$sql = "SELECT * FROM users WHERE id=? AND pass=AES_ENCRYPT(?,'mySecretSalt');";
		$data = array($username, $password);
		$statement = $this->makeStatement($sql, $data);
		//έαν επιστραφεί μοναδική σειρά αποτελεσμάτων σημαίνει πως βρέθηκε αντιστοιχεία username-password
		if($statement->rowCount() === 1) return true;
		else return false;
	}


} // class

?>
